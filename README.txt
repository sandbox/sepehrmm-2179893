
-- SUMMARY --

This modules enables payline.ir payment method for drupal commerce module.

For additional information about payline see their website:
  http://payline.ir


-- REQUIREMENTS --

Just drupal `commerce` and `commerce payment` submodule.


-- INSTALLATION --

* Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7 for further information.



-- CONFIGURATION --

* Configure payline gateway in Administration » Store » Configuration » Payment methods:

  - Find Payline Gateway and click on edit link.

  - In Actions section click on edit link in front of "Enable payment method: Payline Gateway".

  - Fill in required information like api code and gateway addresses which you've obtained from payline.
    * you can select "Debug" checkbox to use payline test api for assuring everything is fine before going live.
* It's possible that the connection cannot be made from your server to payline's server due to problem of self signed certificate of payline's server. In this case you disable SSL certificate verification by checking 'Disable SSL Certifacate Verification' option on configuration page.
  Beware that disable SSL certificate verification has security concerns.